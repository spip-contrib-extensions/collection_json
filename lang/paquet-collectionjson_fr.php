<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/collection_json.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collectionjson_nom' => 'API Collection+JSON',
	'collectionjson_slogan' => 'Implémentation d’une API REST au format JSON pour les objets de SPIP'
);

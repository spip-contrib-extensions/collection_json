<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-collectionjson?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'collectionjson_nom' => 'API Collection+JSON',
	'collectionjson_slogan' => 'Implementação de uma API REST no formato JSON para os objetos do SPIP'
);
